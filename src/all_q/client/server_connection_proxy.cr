require "json"

module AllQ
  class ServerConnectionProxy
    def initialize(server_connection_cache : ServerConnectionCache)
      @server_connection_cache = server_connection_cache
    end

    def send_all(str)
      @server_connection_cache.send_all(str)
      return "{}"
    end

    def aggregate_stats(parsed_data) : String
      @server_connection_cache.aggregate_stats(parsed_data)
    end

    def send_to_server(server_id, data)
      client = server_client(server_id)
      if client
        begin
          result = client.send_string(data)
        rescue ex
          puts "First failed to send_to_server '#{data}'"
          puts ex.inspect_with_backtrace

          # If no server_id was given, it was just sampled from the pool
          # Maybe we can find another to use.
          sampled = server_id.nil?
          result = progressive_backoff(data, client.id, sampled)
        end
      end

      if result.nil?
        raise "Total failure to server."
      end

      return result
    end

    def server_client(server_id)
      if server_id.nil?
        server_client = @server_connection_cache.sample
      else
        server_client = @server_connection_cache.get(server_id)
      end
    end

    def progressive_backoff(hash, server_id, sampled)
      1.upto(3) do |i|
        puts "Retrying send..."
        sleep(1)
        new_client = @server_connection_cache.restart_connection(server_id)

        if !sampled
          val = wrapped_send(new_client, hash)
          # We HAVE to send to this server because we are doing a server specific operation, like DELETE
          return val unless val.nil?
        else
          if new_client && new_client.ping?
            puts "New client looks good!"
            val = wrapped_send(new_client, hash)
            return val unless val.nil?
          else
            # Looks like the server connection is dead, we'll try from another one if we have one
            puts "Trying new sampled client"
            new_client = server_client(nil)
            val = wrapped_send(new_client, hash)
            return val unless val.nil?
            # We'll try a different server
          end
        end
        sleep(i * 2)
      end

      # We have no recourse. Lets kill self and let allq client restart
      @server_connection_cache.kill_us_all

      raise "Failed progressive_backoff"
    end

    def wrapped_send(server_client, hash)
      begin
        return server_client.send_string(hash)
      rescue
        puts "Failed wrapped send. Recreating connection didn't help"
      end
      return nil
    end
  end
end
