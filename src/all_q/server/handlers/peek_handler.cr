require "./base_handler"

# ---------------------------------
# Action: peek
# Params:
#     tube : <tube name> (kick job from tube into ready)
#     buried : true (Check in buried for tube)
# ---------------------------------

module AllQ
  class PeekHandler < BaseHandler
    def process(json : JSON::Any)
      handler_response = HandlerResponse.new("peek")

      data = normalize_json_hash(json)
      offset = data["offset"]? ? data["offset"].to_i : 0

      if data["buried"]? && data["buried"]?.to_s == "true"
        job = @cache_store.buried.peek(data["tube"], offset)
      else
        job = @cache_store.tubes[data["tube"]].peek(offset)
      end

      if job
        handler_response.job = JobFactory.to_hash(job)
      else
        handler_response.job = Hash(String, String).new
      end

      return handler_response
    end
  end
end
