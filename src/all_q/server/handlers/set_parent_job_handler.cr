module AllQ
  class SetParentJobHandler < BaseHandler
    def process(json : JSON::Any)
      handler_response = HandlerResponse.new("set_parent")

      data = normalize_json_hash(json)
      job = JobFactory.build_job_factory_from_hash(json).get_job
      tube_name = data["tube"]

      # Fair queue check
      if @cache_store.fair_queue.is_fair_queue(tube_name)
        tube_name = handle_fair_queue(data, job, tube_name)
        job.tube = tube_name
      end

      timeout = data["timeout"]? ? data["timeout"].to_i : 3600
      run_on_timeout = data["run_on_timeout"]? ? data["run_on_timeout"].downcase == "true" : false

      @cache_store.parents.set_job_as_parent(job, timeout, run_on_timeout)
      if data["limit"]?
        @cache_store.parents.set_limit(job.id, data["limit"].to_i) unless data["limit"].to_s.empty?
      end

      handler_response.job_id = job.id
      handler_response.job = JobFactory.to_hash(job)
      return handler_response
    end

    private def handle_fair_queue(data, job, tube_name)
      shard_key = data["shard_key"]?
      if shard_key.nil?
        raise "Shard key required for Fair Queue (fq-) tubes"
      else
        tube_name = fair_queue_from_put_shard(tube_name, shard_key)
        job.tube = tube_name
        @cache_store.fair_queue.decorate_job(job, @cache_store.tubes)
      end
      tube_name
    end
  end
end
