require "./base_handler"

module AllQ
  class AdminHandler < BaseHandler
    def process(json : JSON::Any) : Hash(String, Hash(String, String))
      return_data = Hash(String, Hash(String, String)).new
      data = normalize_json_hash(json)
      output = Hash(String, String).new
      action_type = data["action_type"]?

      if action_type
        if action_type.to_s == "redirect"
          output["action_type"] = "redirect"
          @cache_store.set_redirect_info(data["server"], data["port"])
        elsif action_type.to_s == "get_reserved_jobs"
          all_jobs = @cache_store.reserved.get_all_jobs
          all_jobs.each do |job|
            output[job.job.id] = JobFactory.to_hash(job.job).to_json
          end
        end
      end

      return_data["result"] = output
      return return_data
    end
  end
end
