#!/bin/bash

echo "Restarting via ENTR"

rm  -rf /tmp/alt/restart/*
pkill -x allq_client
/usr/bin/allq_client &
sleep 1
exit 0

