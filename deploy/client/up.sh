#!/bin/bash

# Watch for kill file, if it appears, restart allq_client because something bad has happened
mkdir -p /tmp/alt/restart
chmod 777 -R  /tmp/alt/restart

./stop_file_watcher.sh &

# Multiprocess allows us to kill the allq_client inside a container without exiting
MULTIPROC="${MULTIPROCESS:-true}"

if [ $MULTIPROC == "true" ]
then
  echo "Multi Process"
  /usr/bin/allq_client &
  while true
  do
   sleep 15
   ./healthcheck.sh
  done
else
  echo "Single Process"
  /usr/bin/allq_client
fi

