require "./spec_helper"

describe AllQ do
  it "should handle releasing" do
    cache_store = AllQ::CacheStore.new
    JobSpec.build_alot_of_jobs(cache_store, 10)
    tube = cache_store.tubes[TEST_TUBE_NAME]
    cache_store.reserved.get_job_ids.size.should eq(0)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(10)

    job = JobSpec.get_via_handler(cache_store, TEST_TUBE_NAME)
    cache_store.reserved.get_job_ids.size.should eq(1)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(9)

    if !job.nil?
      cache_store.reserved.release(job["job_id"], 0)
    else
      raise "Failed go get job"
    end

    cache_store.reserved.get_job_ids.size.should eq(0)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(10)
  end

  it "should handle releasing with delay" do
    cache_store = AllQ::CacheStore.new
    JobSpec.build_alot_of_jobs(cache_store, 10)
    tube = cache_store.tubes[TEST_TUBE_NAME]
    cache_store.reserved.get_job_ids.size.should eq(0)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(10)

    job = JobSpec.get_via_handler(cache_store, TEST_TUBE_NAME)
    cache_store.reserved.get_job_ids.size.should eq(1)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(9)

    if !job.nil?
      cache_store.reserved.release(job["job_id"], 5)
      cache_store.reserved.release(job["job_id"], 5) # Illegal
    else
      raise "Failed go get job"
    end

    sleep(11)
    cache_store.reserved.get_job_ids.size.should eq(0)
    cache_store.tubes[TEST_TUBE_NAME].size.should eq(10)
  end
end
