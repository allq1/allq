require "./spec_helper"

describe AllQ do
  it "should handle getting all reserved" do
    cache_store = AllQ::CacheStore.new
    JobSpec.build_alot_of_jobs(cache_store, 10)
    tube = cache_store.tubes[TEST_TUBE_NAME]
    ah = AllQ::AdminHandler.new(cache_store)

    time_now = Time.utc
    1.upto(10) do
      job = tube.get
      if job
        cache_store.reserved.set_job_reserved(job)
      end
    end

    params = {"action_type" => "get_reserved_jobs"}
    json = JSON.parse(params.to_json)
    output = ah.process(json)
    output["result"].keys.size.should eq(10)
  end
end
